# QR Reader & Generator

## Synapse

### This is a python project intended to generate and read QR Codes for the IEDP final school project

---

## Uses

- This program, in the sate that it is in,isn't useful for most people. I'm still in a trial and error phase in which I'm looking for the best ways in what to do what and at the same time learning how to code in Python

---

## Issues

- Can't generate more than one QR thru a list/array - TBF

---

## Idea

- Every student has its own QR that changes every given time to prevent fake entries/exits.
- The QR contains the current time and the student id.
- When a code is read it is compared to the DB to see which student is entering/leaving.
- Every entry on the system will be turned to 0 at midnight to prevent errors in the next day.
- The students who didn't give an entry or exit in a day will be penalized(TBD).
- Invalid QR entries (like fake ones) will be reported to the class director.

---

## What's working

- The reader can now compare the data witch is read with the data in the database.
- Switched the library for the QR reading successfully. Still haven't tried to generate multiple ones at once.
- Already have a working SQL Query function.
- Design is basically finished.

 ---

## What I'm working on

- Started proof-of-concept UI for the conversion of CSV files into the database.
- Just managed to get the .csv imported and organized in the code.

> This ```for``` gave me cancer.

```csharp
for(int i = 0 ; i < lenght ; i++) {
    for(int ii = 0 ; ii < splits ; ii++) {
        studentArr[i , ii] = studentFile[i]Split( ';' )[ii];  
    }
}
```

---

> Presentation, 26th of March 9am.

---

```python
import meme
def memusMaximus:
    int color = 10

memusMaximus()
```