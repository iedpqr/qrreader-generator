﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AForge;
using AForge.Video;
using AForge.Video.DirectShow;
using ZXing;
using ZXing.Aztec;

namespace QRReader {
    public partial class MainWindow : Form {
        private FilterInfoCollection CaptureDevice;
        private VideoCaptureDevice FinalFrame;
        private List<string> profilepicList = new List<string>();
        private List<string> logg = new List<string>();
        public MainWindow() {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e) {
            CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach(FilterInfo Device in CaptureDevice) {

            }
            FinalFrame = new VideoCaptureDevice(CaptureDevice[0].MonikerString);
            FinalFrame.NewFrame += new NewFrameEventHandler(FinalFrame_NewFrame);
            FinalFrame.Start();
            timer.Start();
        }

        private void FinalFrame_NewFrame(object sender, NewFrameEventArgs eventArgs) {
            try {
                pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
            }
            catch {

            }
        }

        private void timer_Tick(object sender, EventArgs e) {
            dateTime.Text = DateTime.Now.ToString();
            BarcodeReader Readers = new BarcodeReader();
            Result Result;
            try {
            Result = Readers.Decode((Bitmap)pictureBox1.Image);
                string decoded = Result.ToString();
                if(decoded != "") {
                    Data.Text = decoded;
                    List<string> log = new List<string>(File.ReadAllLines("C:\\wamp64\\www\\qrs\\data.csv"));
                    foreach(string line in log) {

                        // This is checks if the student is good to enter the school.
                        if(decoded.ToString() == line) {
                            //System.Threading.Thread.Sleep(200);
                            //MessageBox.Show(decoded.Split(';')[0] + " - Entrou");
                            //if(pb1.Image == null) {
                            //    entryYes.Image = Image.FromFile(@"C:\wamp64\www\qrs\green.png");
                            //    nome1.Text = decoded.Split(';')[0];
                            //    pb1.LoadAsync("http://qrreadergenerator.000webhostapp.com/pfp/"+ decoded.Split(';')[0] + ".png");
                            //    entryYes.Visible = true;
                            //    System.Threading.Thread.Sleep(500);
                            //    entryYes.Visible = false;
                            //
                            //}
                            /*else */
                            if (pb1.Image != null) {
                                pb5.Image = pb4.Image;
                                nome5.Text = nome4.Text;
                                pb4.Image = pb3.Image;
                                nome4.Text = nome3.Text;
                                pb3.Image = pb2.Image;
                                nome3.Text = nome2.Text;
                                pb2.Image = pb1.Image;
                                nome2.Text = nome1.Text;
                                nome1.Text = decoded.Split(';')[0];
                                tbLog.AppendText(decoded.Split(';')[0] + " - Passou às: " + DateTime.Now + "\r\n");
                                //try {
                                    //logg = File.ReadAllLines("C:\\wamp64\\www\\qrs\\log.log").ToList();
                                    //for(int i = 0; i<log.Count;i++) {
                                    //    if(logg[i].Split(';')[0] == decoded.Split(';')[0]) {
                                    //        if (log[i].Split(';')[1] == "In") {
                                    //            tbLog.AppendText(decoded.Split(';')[0] + " - Saiu às: " + DateTime.Now + "\r\n");
                                    //            log.Add(decoded.Split(';')[0] + ";Out; " + DateTime.Now);
                                    //            File.WriteAllLines("C:\\wamp64\\www\\qrs\\log.log", log);
                                    //        }
                                    //        else if(logg[i].Split(';')[1] == "Out") {
                                    //            tbLog.AppendText(decoded.Split(';')[0] + " - Entrou às: " + DateTime.Now + "\r\n");
                                    //            log.Add(decoded.Split(';')[0] + ";In; " + DateTime.Now);
                                    //            File.WriteAllLines("C:\\wamp64\\www\\qrs\\log.log", log);
                                    //        }
                                    //    }
                                    //}
                                    

                                //}
                                //catch {
                                    //log.Add("File init:");
                                    //File.WriteAllLines("C:\\wamp64\\www\\qrs\\log.log", log);
                                //}
                                pb1.LoadAsync("http://qrreadergenerator.000webhostapp.com/pfp/" + decoded.Split(';')[0] + ".png");
                                //entryNot.Image = null;
                                

                            } 
                        }
                        else {
                            //decoded = string.Empty;
                            //entryNot.Image = null;
                            //entryNot.Load(@"C:\wamp64\www\qrs\red.jpg");
                            //System.Threading.Thread.Sleep(500);
                            //entryNot.Image = null;
                        }
                    }
                }
            }
            catch {

            }
        }
        protected override void OnMouseDown(MouseEventArgs e) {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left) {
                this.Capture = false;
                Message msg = Message.Create(this.Handle, 0XA1, new IntPtr(2), IntPtr.Zero);
                this.WndProc(ref msg);
            }
        }

        private void dateTime_Click(object sender, EventArgs e) {

        }

        private void bExit_Click(object sender, EventArgs e) {
            FinalFrame.Stop();
            Environment.Exit(0);
        }

        private void entryNot_Click(object sender, EventArgs e) {

        }

        private void pb1_LoadCompleted(object sender, AsyncCompletedEventArgs e) {

        }

        private void entryNot_LoadCompleted(object sender, AsyncCompletedEventArgs e) {
            //System.Threading.Thread.Sleep(500);
            //entryYes.Image = null;
            //entryYes.InitialImage = null;
            //entryYes.Refresh();
        }
    }
}
