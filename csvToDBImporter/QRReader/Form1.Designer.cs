﻿namespace QRReader {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.Data = new System.Windows.Forms.Label();
            this.nome1 = new System.Windows.Forms.Label();
            this.nome2 = new System.Windows.Forms.Label();
            this.nome3 = new System.Windows.Forms.Label();
            this.nome4 = new System.Windows.Forms.Label();
            this.nome5 = new System.Windows.Forms.Label();
            this.lDateTime = new System.Windows.Forms.Label();
            this.dateTime = new System.Windows.Forms.Label();
            this.lNotification = new System.Windows.Forms.Label();
            this.pb5 = new System.Windows.Forms.PictureBox();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.bMinimize = new System.Windows.Forms.Button();
            this.bExit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbLog = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Data
            // 
            this.Data.AutoSize = true;
            this.Data.Location = new System.Drawing.Point(12, 265);
            this.Data.Name = "Data";
            this.Data.Size = new System.Drawing.Size(38, 17);
            this.Data.TabIndex = 1;
            this.Data.Text = "Data";
            // 
            // nome1
            // 
            this.nome1.AutoSize = true;
            this.nome1.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nome1.Location = new System.Drawing.Point(12, 215);
            this.nome1.Name = "nome1";
            this.nome1.Size = new System.Drawing.Size(118, 34);
            this.nome1.TabIndex = 24;
            this.nome1.Text = "Aluno 1";
            // 
            // nome2
            // 
            this.nome2.AutoSize = true;
            this.nome2.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nome2.Location = new System.Drawing.Point(215, 215);
            this.nome2.Name = "nome2";
            this.nome2.Size = new System.Drawing.Size(118, 34);
            this.nome2.TabIndex = 24;
            this.nome2.Text = "Aluno 2";
            // 
            // nome3
            // 
            this.nome3.AutoSize = true;
            this.nome3.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nome3.Location = new System.Drawing.Point(421, 215);
            this.nome3.Name = "nome3";
            this.nome3.Size = new System.Drawing.Size(118, 34);
            this.nome3.TabIndex = 24;
            this.nome3.Text = "Aluno 3";
            // 
            // nome4
            // 
            this.nome4.AutoSize = true;
            this.nome4.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nome4.Location = new System.Drawing.Point(627, 215);
            this.nome4.Name = "nome4";
            this.nome4.Size = new System.Drawing.Size(118, 34);
            this.nome4.TabIndex = 24;
            this.nome4.Text = "Aluno 4";
            // 
            // nome5
            // 
            this.nome5.AutoSize = true;
            this.nome5.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nome5.Location = new System.Drawing.Point(839, 215);
            this.nome5.Name = "nome5";
            this.nome5.Size = new System.Drawing.Size(118, 34);
            this.nome5.TabIndex = 24;
            this.nome5.Text = "Aluno 5";
            // 
            // lDateTime
            // 
            this.lDateTime.AutoSize = true;
            this.lDateTime.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDateTime.Location = new System.Drawing.Point(1131, 79);
            this.lDateTime.Name = "lDateTime";
            this.lDateTime.Size = new System.Drawing.Size(160, 34);
            this.lDateTime.TabIndex = 24;
            this.lDateTime.Text = "Data/Hora";
            // 
            // dateTime
            // 
            this.dateTime.AutoSize = true;
            this.dateTime.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTime.Location = new System.Drawing.Point(1045, 113);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(160, 34);
            this.dateTime.TabIndex = 24;
            this.dateTime.Text = "Data/Hora";
            this.dateTime.Click += new System.EventHandler(this.dateTime_Click);
            // 
            // lNotification
            // 
            this.lNotification.AutoSize = true;
            this.lNotification.Font = new System.Drawing.Font("Century Gothic", 14.2F);
            this.lNotification.Location = new System.Drawing.Point(1051, 276);
            this.lNotification.Name = "lNotification";
            this.lNotification.Size = new System.Drawing.Size(302, 30);
            this.lNotification.TabIndex = 24;
            this.lNotification.Text = "Notificação de entrada:";
            this.lNotification.Visible = false;
            // 
            // pb5
            // 
            this.pb5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pb5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb5.Location = new System.Drawing.Point(839, 12);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(200, 200);
            this.pb5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb5.TabIndex = 23;
            this.pb5.TabStop = false;
            // 
            // pb4
            // 
            this.pb4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pb4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb4.Location = new System.Drawing.Point(633, 12);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(200, 200);
            this.pb4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb4.TabIndex = 23;
            this.pb4.TabStop = false;
            // 
            // pb3
            // 
            this.pb3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pb3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb3.Location = new System.Drawing.Point(427, 12);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(200, 200);
            this.pb3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb3.TabIndex = 23;
            this.pb3.TabStop = false;
            // 
            // pb2
            // 
            this.pb2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pb2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb2.Location = new System.Drawing.Point(221, 12);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(200, 200);
            this.pb2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb2.TabIndex = 23;
            this.pb2.TabStop = false;
            // 
            // pb1
            // 
            this.pb1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb1.Image = global::QRReader.Properties.Resources.placeholder;
            this.pb1.Location = new System.Drawing.Point(15, 12);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(200, 200);
            this.pb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb1.TabIndex = 23;
            this.pb1.TabStop = false;
            this.pb1.LoadCompleted += new System.ComponentModel.AsyncCompletedEventHandler(this.pb1_LoadCompleted);
            // 
            // bMinimize
            // 
            this.bMinimize.BackColor = System.Drawing.Color.Transparent;
            this.bMinimize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bMinimize.BackgroundImage")));
            this.bMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bMinimize.FlatAppearance.BorderSize = 0;
            this.bMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMinimize.ForeColor = System.Drawing.Color.Transparent;
            this.bMinimize.Location = new System.Drawing.Point(1234, 7);
            this.bMinimize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bMinimize.Name = "bMinimize";
            this.bMinimize.Size = new System.Drawing.Size(57, 57);
            this.bMinimize.TabIndex = 22;
            this.bMinimize.UseVisualStyleBackColor = false;
            // 
            // bExit
            // 
            this.bExit.BackColor = System.Drawing.Color.Transparent;
            this.bExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bExit.BackgroundImage")));
            this.bExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bExit.FlatAppearance.BorderSize = 0;
            this.bExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bExit.ForeColor = System.Drawing.Color.Transparent;
            this.bExit.Location = new System.Drawing.Point(1296, 11);
            this.bExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(57, 57);
            this.bExit.TabIndex = 21;
            this.bExit.UseVisualStyleBackColor = false;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 285);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(584, 324);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(602, 309);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.Size = new System.Drawing.Size(751, 300);
            this.tbLog.TabIndex = 25;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1365, 621);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.dateTime);
            this.Controls.Add(this.lNotification);
            this.Controls.Add(this.lDateTime);
            this.Controls.Add(this.nome5);
            this.Controls.Add(this.nome4);
            this.Controls.Add(this.nome3);
            this.Controls.Add(this.nome2);
            this.Controls.Add(this.nome1);
            this.Controls.Add(this.pb5);
            this.Controls.Add(this.pb4);
            this.Controls.Add(this.pb3);
            this.Controls.Add(this.pb2);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.bMinimize);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.Data);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainWindow";
            this.Text = "Secretaria";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label Data;
        private System.Windows.Forms.Button bMinimize;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.PictureBox pb5;
        private System.Windows.Forms.Label nome1;
        private System.Windows.Forms.Label nome2;
        private System.Windows.Forms.Label nome3;
        private System.Windows.Forms.Label nome4;
        private System.Windows.Forms.Label nome5;
        private System.Windows.Forms.Label lDateTime;
        private System.Windows.Forms.Label dateTime;
        private System.Windows.Forms.Label lNotification;
        private System.Windows.Forms.TextBox tbLog;
    }
}

