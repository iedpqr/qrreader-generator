﻿using System;
using System.Collections.Generic;
using QRCoder;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Text;

namespace QrGenerator {
    public class QrGen {

        static string lastQrLoc = "";
        public static List<string> log = new List<string>();
        

        // This functiong generates the QR codes and uploads them to the server.
        public void generator (string data, string name) {
            #region Logging
            try {
                log = new List<string>(File.ReadAllLines(@"C:\wamp64\www\qrs\data.csv"));
                log.Add( data );
                File.WriteAllLines(@"C:\wamp64\www\qrs\data.csv", log);
            }
            catch(Exception e) {
                Console.WriteLine( e.Message );
            }
            #endregion
            try {
                QRCodeGenerator qrGen = new QRCodeGenerator();
                QRCodeData qrData = qrGen.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrData);
                Bitmap qrImg = qrCode.GetGraphic(20, Color.Black, Color.White, (Bitmap)Bitmap.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\icon.png"));
                lastQrLoc = @"C:\wamp64\www\qrs\" + name;
                Console.WriteLine(lastQrLoc + " - Uploaded");
                
                ftpConnector(lastQrLoc, name);
                
                qrImg.Save(lastQrLoc);
            }
            catch(Exception a) {
                Console.WriteLine(a.Message);
            }
        }

        // This function handles the file transfer protocol.
        public void ftpConnector (string loc, string name) {
            try {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create( "ftp://files.000webhost.com:21/" + @"/public_html/QR/" + name );
                ftpRequest.Credentials = new NetworkCredential( "qrreadergenerator" , "iurirangotangofred" );
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;

                byte[ ] fileContent;  //in this array you'll store the file's content
                fileContent = File.ReadAllBytes( loc );
                using (Stream sw = ftpRequest.GetRequestStream()) {
                    sw.Write( fileContent , 0 , fileContent.Length ); 
                    //sending the content to the FTP Server
                }
                Console.WriteLine("Uploaded.");
            }
            catch(Exception e) {
                Console.WriteLine( e.Message );
            }
        }

        // This function deletes the previous generated QR.
        public void ftpDeleter(string loc, string name) {
            try {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create("ftp://files.000webhost.com:21/" + @"/public_html/QR/" + name);
                ftpRequest.Credentials = new NetworkCredential("qrreadergenerator", "iurirangotangofred");
                ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;

                byte[] fileContent;  //in this array you'll store the file's content
                fileContent = File.ReadAllBytes(loc);
                using (Stream sw = ftpRequest.GetRequestStream()) {
                    sw.Write(fileContent, 0, fileContent.Length);  //sending the content to the FTP Server
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

    }
}
