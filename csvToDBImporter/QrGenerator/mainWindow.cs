﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using QRCoder;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace QrGenerator {
    public partial class mainWindow : Form {
        public mainWindow () {
            InitializeComponent();
        }

        private void bExit_Click ( object sender , EventArgs e ) {
            Application.Exit();
        }

        private void bMinimize_Click ( object sender , EventArgs e ) {
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.WindowState = FormWindowState.Minimized;
        }

        private void mainWindow_Resize ( object sender , EventArgs e ) {
            if (this.WindowState == FormWindowState.Minimized) {
                Hide();
                trayIcon.Visible = true;
                trayIcon.ShowBalloonTip(30);
                
            }
        }

        private void trayIcon_DoubleClick ( object sender , EventArgs e ) {
            Show();
            this.WindowState = FormWindowState.Normal;
            trayIcon.Visible = false;
        }
        protected override void OnMouseDown ( MouseEventArgs e ) {
            base.OnMouseDown( e );
            if (e.Button == MouseButtons.Left) {
                this.Capture = false;
                Message msg = Message.Create( this.Handle , 0XA1 , new IntPtr( 2 ) , IntPtr.Zero );
                this.WndProc( ref msg );
            }
        }

        private void bStart_Click ( object sender , EventArgs e ) {
            tick_Tick(sender, e);
            tick.Start();
            
            bStart.Enabled = false;
            bStop.Enabled = true;
            try {
               //SqlConnector.connClose();
               //SqlConnector sqC = new SqlConnector();
               //string query = "SELECT * FROM studenttable";
               //MySqlDataReader reader = sqC.SQL( query );
               //while (reader.Read()) {
               //    tbInfo.AppendText( reader["nome"].ToString() );
               //}
            }
            catch(Exception a) {
                //tbInfo.AppendText( a.Message );
            }
        }

        private void tick_Tick ( object sender , EventArgs e ) {
            string data = "";
            string name = "";
            // This part clears the previous information that was on the CSV file.
            var log = new List<string>();
            log = new List<string>(File.ReadAllLines(@"C:\wamp64\www\qrs\data.csv"));
            log.Clear();
            File.WriteAllLines(@"C:\wamp64\www\qrs\data.csv", log);

            try {
                SqlConnector.connClose();
                SqlConnector sqC = new SqlConnector();
                string query = "SELECT * FROM studenttable";
                MySqlDataReader reader = sqC.SQL( query );
                while (reader.Read()) {
                    name =  reader["username"].ToString() ;
                    name = name.Replace( ' ' , '_' );
                    data = reader["username"].ToString();
                    data = data + ";" + RoundUp(DateTime.Now, TimeSpan.FromMinutes(30));
                    data = data.ToString();
                    name = name.ToString();

                    

                    // This calls the generator function which also stores data for later reading.
                    QrGen gen = new QrGen();
                    gen.generator(data, name + ".png");
                    tbInfo.AppendText(name + " @ " + RoundUp(DateTime.Now, TimeSpan.FromMinutes(30)) + " - Generated");
                    tbInfo.AppendText(Environment.NewLine);
                }
            } 
            catch (Exception a) {
                tbInfo.AppendText( a.Message );
            }
        }

        private void mainWindow_Load ( object sender , EventArgs e ) {

        }

        private void bStop_Click ( object sender , EventArgs e ) {
            bStart.Enabled = true;
            bStop.Enabled = false;
            tick.Stop();
        }
        DateTime RoundUp ( DateTime dt , TimeSpan d ) {
            return new DateTime( (dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks , dt.Kind );
        }

        private void tbInfo_TextChanged(object sender, EventArgs e) {

        }
    }
}
