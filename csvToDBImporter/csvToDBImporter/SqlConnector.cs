﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace csvToDBImporter {

    public class SqlConnector {
        public static string server = "https://qrreadergenerator.000webhostapp.com/";
        public static string database = "students";
        public static string uid = "keyrteam";
        public static string password = "iurirangotangofred";

        public static string connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

        private static MySqlConnection mConn = new MySqlConnection( connectionString );
        private DataSet mDataSet;

        public MySqlDataReader SQL ( string sql ) {
            if (mConn.State == ConnectionState.Closed) {
                connOpen();
                MySqlDataReader dR;
                MySqlCommand cmd = new MySqlCommand( sql , mConn );
                try {
                    cmd.ExecuteNonQuery();
                    dR = cmd.ExecuteReader();
                    return dR;
                }
                catch (Exception e) {
                    MessageBox.Show( e.Message );
                    return null;
                }
            }
            else {
                MySqlDataReader dR;
                MySqlCommand cmd = new MySqlCommand( sql , mConn );
                try {
                    cmd.ExecuteNonQuery();
                    dR = cmd.ExecuteReader();
                    return dR;
                }
                catch (Exception e) {
                    MessageBox.Show( e.Message );
                    return null;
                }
            }
        }

        public static void connOpen () {
            mConn.Open();
        }

        public static void connClose () {
            mConn.Close();
        }
    }
}