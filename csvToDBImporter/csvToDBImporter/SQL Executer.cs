﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace csvToDBImporter {

    public partial class SQL_Executer : Form {

        public SQL_Executer () {
            InitializeComponent();
            Application.OpenForms[0].Enabled = false;
        }

        //This function makes the form draggable from any empty space in it.
        protected override void OnMouseDown ( MouseEventArgs e ) {
            base.OnMouseDown( e );
            if (e.Button == MouseButtons.Left) {
                this.Capture = false;
                Message msg = Message.Create( this.Handle , 0XA1 , new IntPtr( 2 ) , IntPtr.Zero );
                this.WndProc( ref msg );
            }
        }

        private void bExit_Click ( object sender , EventArgs e ) {
            this.Close();
            Application.OpenForms[0].Enabled = true;
        }

        private void bMinimize_Click ( object sender , EventArgs e ) {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bExec_Click ( object sender , EventArgs e ) {
            try {
                SqlConnector.connClose();
                SqlConnector sqC = new SqlConnector();
                MySqlDataReader dR = sqC.SQL( tbInput.Text );
                while (dR.Read()) {
                    tbOutput.AppendText(
                        dR["processnr"] + " "
                        + dR["username"] + " "
                        + dR["name"] + " "
                        + dR["password"] + " "
                        + dR["class"] + "\r\n"
                        );
                }
            }
            catch (Exception ex) {
                tbOutput.AppendText( "Error: " + ex.Message + "\r\n" );
            }
        }

        private void SQL_Executer_Load(object sender, EventArgs e) {

        }
    }
}