﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace csvToDBImporter {

    public partial class mainAct : Form {
        public static string server = "localhost";
        public static string database = "students";
        public static string uid = "keyrteam";
        public static string password = "iurirangotangofred";

        public static string connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

        //private static MySqlConnection mConn = new MySqlConnection( connectionString );
        private DataSet mDataSet;

        public mainAct () {
            InitializeComponent();
        }

        private static string[ , ] studentArr;

        private static void Populator () {
            var studentFile = File.ReadAllLines( dataLocation + "teste.csv" , Encoding.UTF8 );
            int length = studentFile.Length;
            int splits = studentFile[0].Split( ';' ).Length;
            studentArr = new string[length , splits];
            for (int i = 0 ; i < length ; i++) {
                for (int ii = 0 ; ii < splits ; ii++) {
                    studentArr[i , ii] = studentFile[i].Split( ';' )[ii];
                }
            }
        }

        //This string is a placeholder for a Select Folder Dialog
        //public static string dataLocation = "C:\\Users\\Iuri\\Documents\\Repos\\QrReader&Generator\\";
        public static string dataLocation = "C:\\Users\\Iuri\\Documents\\Repos\\QrReader&Generator\\";

        private void Form1_Load ( object sender , EventArgs e ) {

            #region csvImporter

            Populator();
            foreach (var line in studentArr) {
                Console.WriteLine( line + " - This is in the csv." );
            }

            #endregion csvImporter
        }

        //This function receives SQL inputs as queries.
        //(uid text, usern text, name text, passw text, turma text) - This is how the DB is organized.

        //This function makes the form draggable from any empty space in it.
        protected override void OnMouseDown ( MouseEventArgs e ) {
            base.OnMouseDown( e );
            if (e.Button == MouseButtons.Left) {
                this.Capture = false;
                Message msg = Message.Create( this.Handle , 0XA1 , new IntPtr( 2 ) , IntPtr.Zero );
                this.WndProc( ref msg );
            }
        }

        //This function closes the app when the close button is pressed.
        private void bExit_Click ( object sender , EventArgs e ) {
            Application.Exit();
        }

        private void bMinimize_Click ( object sender , EventArgs e ) {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.WindowState = FormWindowState.Minimized;
        }

        //This button get's the last database selected and populates the app.
        private void bReload_Click ( object sender , EventArgs e ) {
            SqlConnector.connClose();
            SqlConnector sqC = new SqlConnector();
            MySqlDataReader thisReader = sqC.SQL( "SELECT * FROM `studenttable`" );

            while (thisReader.Read()) {
                Console.WriteLine( thisReader["processnr"] + " é o cona" );
                cbSelect.Items.Add( thisReader["username"] );
            }
        }

        private void bLoad_Click ( object sender , EventArgs e ) {
            SqlConnector.connClose();
            SqlConnector sqC = new SqlConnector();
            string selectedStudent = "select * from studenttable where username='" + cbSelect.Text + "'";
            Console.WriteLine( "In the combo box - " + cbSelect.Text );
            MySqlDataReader thisReader = sqC.SQL( selectedStudent );
            while (thisReader.Read()) {
                Console.WriteLine( "In the DB - " + thisReader["username"] + " ... In the TB - " + cbSelect.Text );
                tbName.Text = thisReader["name"].ToString();
                tbPassw.Text = thisReader["password"].ToString();
                tbUsern.Text = thisReader["username"].ToString();
                tbClass.Text = thisReader["class"].ToString();
                tbUserID.Text = thisReader["processnr"].ToString();
            }
        }

        private void bSqlCmd_Click ( object sender , EventArgs e ) {
            SQL_Executer SqlCmd = new SQL_Executer();
            SqlCmd.Show();
        }

        private void mainAct_Activated ( object sender , EventArgs e ) {
            //mainAct.ActiveForm.FormBorderStyle = FormBorderStyle.None;
        }

        private void bSearch_Click ( object sender , EventArgs e ) {
            SqlConnector.connClose();
            SqlConnector sqC = new SqlConnector();
            string queru = tbSearch.Text;
            string selectedStudent = "SELECT * FROM studenttable WHERE name LIKE " + "'" + "%" + queru + "%" + "'";
            MySqlDataReader thisReader = sqC.SQL( selectedStudent );
            try {
                while (thisReader.Read()) {
                    //Console.WriteLine( thisReader["name"].ToString() + "- tests" );
                    for (int i = 0 ; i < cbSelect.Items.Count ; i++) {
                        try {
                            if (thisReader["username"].ToString() == cbSelect.Items[i].ToString()) {
                                cbSelect.SelectedIndex = i;
                                bLoad_Click( sender , e );
                                break;
                            }
                        }
                        catch (Exception asd) {
                            MessageBox.Show( asd.Message );
                        }
                    }
                }
            }
            catch(Exception whiles) {
                Console.WriteLine( whiles.Message );

            }
        }

        private void mainAct_FormClosing ( object sender , FormClosingEventArgs e ) {
            //mConn.Close();
        }
    }
}