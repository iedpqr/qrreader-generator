﻿namespace csvToDBImporter {
    partial class SQL_Executer {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing ) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SQL_Executer));
            this.bExec = new System.Windows.Forms.Button();
            this.bMinimize = new System.Windows.Forms.Button();
            this.bExit = new System.Windows.Forms.Button();
            this.tbInput = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bExec
            // 
            this.bExec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(153)))), ((int)(((byte)(219)))));
            this.bExec.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bExec.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bExec.FlatAppearance.BorderSize = 5;
            this.bExec.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(33)))), ((int)(((byte)(61)))));
            this.bExec.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(153)))));
            this.bExec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bExec.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bExec.ForeColor = System.Drawing.Color.Transparent;
            this.bExec.Location = new System.Drawing.Point(721, 14);
            this.bExec.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bExec.Name = "bExec";
            this.bExec.Size = new System.Drawing.Size(211, 59);
            this.bExec.TabIndex = 2;
            this.bExec.Text = "Executar";
            this.bExec.UseVisualStyleBackColor = false;
            this.bExec.Click += new System.EventHandler(this.bExec_Click);
            // 
            // bMinimize
            // 
            this.bMinimize.BackColor = System.Drawing.Color.Transparent;
            this.bMinimize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bMinimize.BackgroundImage")));
            this.bMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bMinimize.FlatAppearance.BorderSize = 0;
            this.bMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMinimize.ForeColor = System.Drawing.Color.Transparent;
            this.bMinimize.Location = new System.Drawing.Point(937, 10);
            this.bMinimize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bMinimize.Name = "bMinimize";
            this.bMinimize.Size = new System.Drawing.Size(57, 57);
            this.bMinimize.TabIndex = 20;
            this.bMinimize.UseVisualStyleBackColor = false;
            this.bMinimize.Click += new System.EventHandler(this.bMinimize_Click);
            // 
            // bExit
            // 
            this.bExit.BackColor = System.Drawing.Color.Transparent;
            this.bExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bExit.BackgroundImage")));
            this.bExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bExit.FlatAppearance.BorderSize = 0;
            this.bExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bExit.ForeColor = System.Drawing.Color.Transparent;
            this.bExit.Location = new System.Drawing.Point(1000, 14);
            this.bExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(57, 57);
            this.bExit.TabIndex = 19;
            this.bExit.UseVisualStyleBackColor = false;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // tbInput
            // 
            this.tbInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(153)))), ((int)(((byte)(219)))));
            this.tbInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbInput.Font = new System.Drawing.Font("Monaco", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInput.ForeColor = System.Drawing.Color.White;
            this.tbInput.Location = new System.Drawing.Point(15, 25);
            this.tbInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbInput.Name = "tbInput";
            this.tbInput.Size = new System.Drawing.Size(701, 44);
            this.tbInput.TabIndex = 21;
            // 
            // tbOutput
            // 
            this.tbOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(153)))), ((int)(((byte)(219)))));
            this.tbOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOutput.Font = new System.Drawing.Font("Fira Code Medium", 13F, System.Drawing.FontStyle.Bold);
            this.tbOutput.ForeColor = System.Drawing.Color.White;
            this.tbOutput.Location = new System.Drawing.Point(15, 74);
            this.tbOutput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.Size = new System.Drawing.Size(1042, 397);
            this.tbOutput.TabIndex = 22;
            // 
            // SQL_Executer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(1072, 485);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.tbInput);
            this.Controls.Add(this.bMinimize);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.bExec);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SQL_Executer";
            this.Text = "SQL_Executer";
            this.Load += new System.EventHandler(this.SQL_Executer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bExec;
        private System.Windows.Forms.Button bMinimize;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.TextBox tbInput;
        private System.Windows.Forms.TextBox tbOutput;
    }
}