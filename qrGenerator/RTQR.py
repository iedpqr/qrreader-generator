from pyzbar.pyzbar import *
from PIL import Image
import cv2
import winsound
import mysql.connector
import ctypes
import threading
import time
import urllib.request
from io import BytesIO
try:
    # Python2
    import Tkinter as tk
    from urllib2 import urlopen
except ImportError:
    # Python3
    import tkinter as tk
    from urllib.request import urlopen


def main():


    # Begin capturing video. You can modify what video source to use with VideoCapture's argument. It's currently set
    # to be your webcam.
    capture = cv2.VideoCapture(0)
    #dbConnect()
    while True:
        # To quit this program press q.
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        # Breaks down the video into frames
        ret, frame = capture.read()

        # Displays the current frame
        cv2.imshow('Current', frame)

        # Converts image to grayscale.
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Uses PIL to convert the grayscale image into a ndary array that ZBar can understand.
        image = Image.fromarray(gray)
        zbar_decoded = decode(image)


        # Prints data from image.
        for decoded in zbar_decoded:
                print(decoded.data)
                #region This next part is where I treat the read values into simple strings
                data = str(decoded.data)
                data = data.replace("'","")
                dataL = list(data)
                del(dataL[0])
                data = "".join(dataL)
                #endregion
                print(data) # This prints username;datetime
                log = open("C:\wamp64\www\qrs\data.csv","r")
                finaldata = data.replace(" ","")
                for line in log:
                    dataList = list(finaldata)
                    finalLine = line.replace(" ", "")
                    lineList = list(finalLine)
                    del lineList[-1]
                    if(dataList == lineList):
                        aluno = data.split(';')[0]
                        print("Boa Escola " + aluno)
                        frequency = 700
                        duration = 500
                        winsound.Beep(frequency, duration)
                        w = tk.Tk()
                        var = tk.StringVar()
                        url = ('http://qrreadergenerator.000webhostapp.com/pfp/'+aluno+'.png')
                        #u = urllib.urlrequest.urlopen(url)
                        urllib.request.urlretrieve(url, "C:\wamp64\www\pp"+'\\' + aluno + '.png')
                        #image1 = ImageTk.PhotoImage(im)
                        #photo = tk.PhotoImage(data=image_b64)
                        l = tk.Label(w, textvariable=var,font=("Century Gothic", 20),bg="green",image="C:\wamp64\www\pp/" + aluno + '.png')
                        var.set("Boa Escola "+ aluno)
                        l.pack()
                        w.after(2000, lambda: w.destroy()) # Destroy the widget after 30 seconds
                        w.mainloop()
                
                

def worker(title,close_until_seconds):
    time.sleep(close_until_seconds)
    wd=ctypes.windll.user32.FindWindowA(0,title)
    ctypes.windll.user32.SendMessageA(wd,0x0010,0,0)
    return

def AutoCloseMessageBoxW(text, title, close_until_seconds):
    t = threading.Thread(target=worker,args=(title,close_until_seconds))
    t.start()
    ctypes.windll.user32.MessageBoxA(0, text, title, 0)

    # TODO
    # Create database and insert read values.
    # Find a way to decode the QR read hash.


#This function compares the read values with the values in the database.
#def dbConnect():
#    mydb = mysql.connector.connect(
#    host="localhost",
#    user="iuribdias",
#    passwd="Iuribatdias111",
#    database="studentdb"
#    )
#    mycursor = mydb.cursor()
#    mycursor.execute("SELECT * FROM studenttable")
#    myresult = mycursor.fetchall()
#    for i in myresult:
#        print(i)
#


if __name__ == "__main__":
   main()