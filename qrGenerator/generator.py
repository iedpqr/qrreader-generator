import qrcode
#import image

qr = qrcode.QRCode(
    version=2,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=50,
    border=1,
)

# This data will change based on input.
# Data structure that allows for automatic contact adding in IPhones "MECARD:N:John Doe;TEL:555-555-5555;EMAIL:email@example.com;NOTE:Contoso;URL:http://www.example.com;"

data = [["13465", "Iuri Dias"],["12412", "Xico Moreira"]]
count = 0
for student in data:
    qr.data_list.clear
    qr.add_data(student) #TODO Either change this from 'add' to 'set' in some way.
    qr.make(fit=True)
    img = qr.make_image(fill_color="darkblue", back_color="#e3e3e3")
    qr.data_list = qr.data_list.clear    
    img.save("D:\Documents\Repos\qrreader-generator\QRs\\"+student[1]+".png")
    print(qr.data_list) #FIXME Or get a way to clear the data without it getting completely retarded.
    
# This ^ print outputs this:
# [b"['13465', 'Iuri Dias']"]
# [b"['13465', 'Iuri Dias']", b"['12412', 'Xico Moreira']"]
# Basically the second QR goes with 2 entries instead of the intended 1.
    
    
    


