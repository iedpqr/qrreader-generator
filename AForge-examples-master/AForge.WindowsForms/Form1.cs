﻿using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ZXing;
using System.Threading;
using System.Threading.Tasks;
namespace AForge.WindowsForms {

    public partial class Form1 : Form {
        private FilterInfoCollection videoDevicesList;
        private IVideoSource videoSource;

        public Form1() {
            InitializeComponent();
            // get list of video devices
            videoDevicesList = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo videoDevice in videoDevicesList) {
                cmbVideoSource.Items.Add(videoDevice.Name);
            }
            if (cmbVideoSource.Items.Count > 0) {
                cmbVideoSource.SelectedIndex = 0;
            }
            else {
                MessageBox.Show("No video sources found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // stop the camera on window close
            this.Closing += Form1_Closing;
        }

        private void Form1_Closing(object sender, CancelEventArgs e) {
            // signal to stop
            if (videoSource != null && videoSource.IsRunning) {
                videoSource.SignalToStop();
            }
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs) {
            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            pictureBox1.Image = bitmap;
            
        }

        private void btnStart_Click(object sender, EventArgs e) {
            videoSource = new VideoCaptureDevice(videoDevicesList[cmbVideoSource.SelectedIndex].MonikerString);
            videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);
            videoSource.Start();
        }

        private void btnStop_Click(object sender, EventArgs e) {
            videoSource.SignalToStop();
            if (videoSource != null && videoSource.IsRunning && pictureBox1.Image != null) {
                pictureBox1.Image.Dispose();
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            btnStart_Click(sender, e);
            timer.Start();
        }

        private void decode_QRtag() {
            try {
                //pictureBox1 shows the web cam video
                Bitmap bitmap = new Bitmap(pictureBox1.Image);
                BarcodeReader reader = new BarcodeReader { AutoRotate = true, TryInverted=true };
                Result result = reader.Decode(bitmap);
                string decoded = result.ToString().Trim();
                //capture a snapshot if there is a match
                pictureBox1.Image = bitmap;
                textBox1.Text = decoded;
            }
            catch {
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

        }

        private void timer_Tick(object sender, EventArgs e) {
            btnStop_Click(sender, e);
            decode_QRtag();
            btnStart_Click(sender, e);
        }
    }
}